import constants
import base.colors
from ghosts.ghost import Ghost
from ghosts.ghost import init_ghost_animations

from base.spritesheet import SpriteSheet

class Blinky(Ghost):
    sprite_sheet = SpriteSheet("images/pacman-sprites.png")
    blinky_animations = init_ghost_animations(sprite_sheet, 4)

    def __init__(self, game, x, y):
        super().__init__(game, x,y,self.blinky_animations, (26*8+4,0+4))
        self.color = base.colors.RED

    def update_target_pos(self):
        # Only need to update the target tile if we are in chase mode
        # For scatter and dead states, target tile is fixed (corner or ghost house)
        if self.state == Ghost.STATE_CHASE:
            self.target_pos = self.game.hero.position()
