import base.colors
from ghosts.ghost import Ghost
from ghosts.ghost import init_ghost_animations

from base.spritesheet import SpriteSheet

class Inky(Ghost):
    sprite_sheet = SpriteSheet("images/pacman-sprites.png")
    inky_animations = init_ghost_animations(sprite_sheet, 6)

    def __init__(self, game, x, y):
        super().__init__(game, x,y,self.inky_animations, (28*8-4,35*8+4))
        self.color = base.colors.CYAN

