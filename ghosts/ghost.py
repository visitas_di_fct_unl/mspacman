import constants
from base.mobile import Mobile
from random import choice
from math import sqrt

import pygame
import base.colors
import sys

class Ghost(Mobile):
    """
    Ghost class
    
    A class for Ms.Pac-man and Pac-man ghosts.
    """

    # Constants for each ghost state
    STATE_FRIGHTENED = 0
    STATE_RECOVERING = 1
    STATE_CHASE = 2
    STATE_SCATTER = 3
    STATE_DEAD = 4

    # Constants for several speeds, depending on the current state
    NORMAL_SPEED = 0.7 * constants.FPS
    SLOW_SPEED = 0.5*constants.FPS
    DEAD_SPEED = constants.FPS

    # Constants for each ghost animation
    ANIM_FRIGHTENED = 0
    ANIM_RECOVERING = 1
    ANIM_DEAD_LEFT = 2
    ANIM_DEAD_RIGHT = 3
    ANIM_DEAD_UP = 4
    ANIM_DEAD_DOWN = 5
    ANIM_LEFT = 6
    ANIM_RIGHT = 7
    ANIM_UP = 8
    ANIM_DOWN = 9

    # number of seconds that a ghost will remain frightened
    FRIGHTENED_TIME = 10
    # number of seconds that a ghost will remain in recovery
    RECOVERY_TIME = 3

    # number of update() calls to exit frightened mode
    FRIGHTENED_FRAMES = constants.FPS * FRIGHTENED_TIME
    # number of update() calls to exit recovery mode
    RECOVERY_FRAMES = constants.FPS * RECOVERY_TIME

    def __init__(self, game, x, y, animations, scatter_pos):
        """
        Ghost constructor
        :param game: the game object
        :param x: the x position of the top left corner of the image on screen
        :param y: the y position of the top left corner of the image on screen
        :param animations: the set of sets of images that make up the animations 
        :param scatter_pos: the target position for the ghost while in state STATE_SCATTER 
        """
        super().__init__(game, x, y, animations)
        # Locked ghosts are inside the ghosts' house
        self.locked = True
        # Countdown for timed animations (ANIM_FRIGHTENED and ANIM_RECOVERING)
        self.countdown = 0
        # the current state of the ghost
        self.state = None
        # the last state of the ghost (when frightened, previous state is stored here)
        self.last_state = None
        # the current target_position for the ghost. A ghost will always try to reach this position
        # while in state STATE_CHASE.
        self.target_pos = (self.rect.x, self.rect.y)
        # the monster target_position while in state SCATTER_STATE
        self.scatter_target_pos = scatter_pos
        # The monster color (used for drawing auxiliary line showing target_pos)
        self.color = base.colors.WHITE

        # Set the initial speed, direction and mode.
        self.set_speed(Ghost.NORMAL_SPEED)
        self.face_dir(Mobile.DIR_LEFT)
        self.scatter()

        self.debug = False


    def is_killer(self):
        """
        A ghost is a hero killer, so it returns True when asked if it is a killer.
        :return: True
        """
        return True


    def unlock(self):
        """
        Unlocks the ghost from the ghosts' house.
        """
        self.locked = False

    def is_locked(self):
        """
        Checks whether the ghost is locked.
        :return: True if the monster is locked inside the ghosts' house, False otherwise
        """
        return self.locked

    def frighten(self):
        """ 
        Frightens the ghost
        
        The ghost will become vulnerable to the hero for a short time, followed by a period
        in which it is recovering.
        """
        # Display the ANIM_FRIGHTENED animation
        self.start_animation(Ghost.ANIM_FRIGHTENED)

        # Start counting down the frames until the end of this animation
        self.countdown = Ghost.FRIGHTENED_FRAMES

        # records the previous ghost state, unless the ghost is still frightened
        if not self.is_frightened():
            self.last_state = self.state

        # sets the current state to STATE_FRIGHTENED
        self.state = Ghost.STATE_FRIGHTENED

        # slows down the ghost speed
        self.set_speed(Ghost.SLOW_SPEED)

    def is_frightened(self):
        """
        Checks if the ghost is in STATE_FRIGHTNED
        :return: True if the ghost is frightened, False otherwise.
        """
        return self.state == Ghost.STATE_FRIGHTENED

    def die(self):
        """
        Kills the ghost.
        
        During this mode the ghost will return to the ghosts' house at full speed.
        """

        # set the state
        self.state = Ghost.STATE_DEAD
        # define the current target position
        self.target_pos = self.game.ghosts_house_pos()
        # full speed ahead (dead speed)
        self.set_speed(Ghost.DEAD_SPEED)

    def is_dead(self):
        """
        Checks if the ghost is currently dead.
        
        :return: True if the ghost is dead, False otherwise.
        """
        return self.state == Ghost.STATE_DEAD

    def chase(self):
        """
        Switches the ghost to state STATE_CHASE
        """

        # sets the current state
        self.state = Ghost.STATE_CHASE
        # also sets the previous state to return to chase state.
        #self.last_state = Ghost.STATE_CHASE

    def is_chasing(self):
        """
        Checks whether the ghost is in state STATE_CHASE
        :return: True if in chase mode, False otherwise
        """
        return self.state == Ghost.STATE_CHASE

    def scatter(self):
        """
        Switches the ghost to scatter mode.
        """
        self.state = Ghost.STATE_SCATTER
        self.last_state = Ghost.STATE_SCATTER
        self.target_pos = self.scatter_target_pos

    def is_scattering(self):
        """
        Checks whether the ghost is in state STATE_SCATTER.
        :return: True if in scatter mode, False otherwise.
        """
        return self.state == Ghost.STATE_SCATTER

    def draw(self, screen):
        """
        Draws the ghost on a given surface
        :param screen: the surface where to draw the ghost
        """
        if self.debug and (self.is_chasing() or self.is_scattering()):
            pygame.draw.line(screen, self.color, self.position(), self.target_pos)
        super().draw(screen)

    def update(self):
        """
        The ghost's frame by frame logic.
        
        The execution of this operation will trigger the following event sequence:
        
        i) adjust_animations(), to select the current ghost animation depending on its internal state
        i) update_position(), to determine where the monster will be facing at the end of this frame
        ii) update_animation(), from Animated class to control the playback of the current animation
        :return: 
        """
        self.adjust_animations()
        super().update() # this will call: i. update_position() ii. update_animation()

    # GIRLS IN ICT
    def update_position(self):
        if self.is_locked():
            return

        if self.is_frightened():
            self.do_frightened_work()
        else:
            self.update_target_pos()
            self.chase_target_pos()

        self.move()

    def update_target_pos(self):
        """
        Updates the current target position of the ghost. A generic ghost does nothing.
        Each specific ghost type will need to redefine this operation.

        Note: This only needs to be performed while in chase mode. in scatter mode the
              ghost's target it fixed and in scatter mode, the decision is random at each
              crossing.
        """
        pass


    def chase_target_pos(self):
        """
        In this step, chasing the hero is to compute all possible directions
        and then select the direction that most reduces the distance to the target.

        Finding the distance to future positions is done by function distance_to, knowing 
        the direction to take, and the position of the target.
        """
        directions = self.get_directions()

        best_distance = sys.maxsize # A very large number
        best_dir = None

        for dir in directions:
            dist = self.distance_to(dir, self.target_pos)
            if dist < best_distance:
                best_distance = dist
                best_dir = dir

        self.face_dir(best_dir)

    def distance_to(self, dir, target):
        center = self.position()
        dx = (center[0] + dir[0]) - target[0]
        dy = (center[1] + dir[1]) - target[1]
        return sqrt(dx * dx + dy * dy)








    """
    From this point onwards you can find some internal functions. Ignore them!
    """

    def adjust_animations(self):
        """
        This operation will adust the current animation to match the ghost's internal state
        and its current direction.
        """
        if self.countdown == 0:
            if self.curr_anim == Ghost.ANIM_FRIGHTENED:
                self.recover()
                return
            elif self.curr_anim == Ghost.ANIM_RECOVERING:
                self.heal()
        self.countdown = self.countdown - 1

        if self.state == Ghost.STATE_CHASE or self.state == Ghost.STATE_SCATTER:
            if self.is_facing_up() and self.curr_anim != Ghost.ANIM_UP:
                self.start_animation(Ghost.ANIM_UP)
            elif self.is_facing_down() and self.curr_anim != Ghost.ANIM_DOWN:
                self.start_animation(Ghost.ANIM_DOWN)
            elif self.is_facing_left() and self.curr_anim != Ghost.ANIM_LEFT:
                self.start_animation(Ghost.ANIM_LEFT)
            elif self.is_facing_right() and self.curr_anim != Ghost.ANIM_RIGHT:
                self.start_animation(Ghost.ANIM_RIGHT)

        elif self.state == Ghost.STATE_DEAD:
            if self.is_facing_up() and self.curr_anim != Ghost.ANIM_DEAD_UP:
                self.start_animation(Ghost.ANIM_DEAD_UP)
            elif self.is_facing_down() and self.curr_anim != Ghost.ANIM_DEAD_DOWN:
                self.start_animation(Ghost.ANIM_DEAD_DOWN)
            elif self.is_facing_left() and self.curr_anim != Ghost.ANIM_DEAD_LEFT:
                self.start_animation(Ghost.ANIM_DEAD_LEFT)
            elif self.is_facing_right() and self.curr_anim != Ghost.ANIM_DEAD_LEFT:
                self.start_animation(Ghost.ANIM_DEAD_RIGHT)


    def get_directions(self):
        """
        Gets all the directions that are possible moves for a Ghost actor. 
        These are the directions that will not make the actor collide with another one.
        Ghosts cannot invert their direction at will, so the get_directions() operation
        on Mobile class needs to be redefined for Ghosts specific behavior.

        :return: A list of all the directions that are collision free. Each direction is a pair (dx,dy)
            where dx and dy can either be 0, 1 or -1.
            If no move is possible in any of the 4 principal directions, DIR_NONE (0,0) 
            will be returned in the list.
        """

        # Start with an empty list
        directions = []

        # Check each direction and put it in the list

        # One particular detail in
        if self.can_go_left() and not self.is_facing_right():
            directions.append(Mobile.DIR_LEFT)
        if self.can_go_right() and not self.is_facing_left():
            directions.append(Mobile.DIR_RIGHT)
        if self.can_go_up() and not self.is_facing_down():
            directions.append(Mobile.DIR_UP)
        if self.can_go_down() and not self.is_facing_up():
            directions.append(Mobile.DIR_DOWN)

        if directions == []:
            directions.append(Mobile.DIR_NONE)


        # Special cases to cross the ghosts' house door
        pos = self.position()
        if self.is_dead():
            if pos[0] == 112 and pos[1] >= 116 and pos[1] <= 131:
                directions = [Mobile.DIR_DOWN]
            elif pos[0] == 112 and pos[1] == 132:
                self.heal()
                
        elif not self.is_locked():
            if pos[0] == 112 and pos[1] > 116 and pos[1] <= 132:
                directions = [Mobile.DIR_UP]
            elif pos[0] >= 96 and pos[0] <= 128 and pos[1] > 140 and pos[1] <= 148:
                directions = [Mobile.DIR_UP]

        return directions


    # A monster in frightened mode will select a way to go randomly
    def do_frightened_work(self):
        if self.is_locked():
            return
        directions = self.get_directions()
        if len(directions)>0:
            self.face_dir(choice(directions))
        else:
            self.face_dir(Mobile.DIR_NONE)

    # Call this to start ghost recovery
    def recover(self):
        self.start_animation(Ghost.ANIM_RECOVERING)
        self.countdown = Ghost.RECOVERY_FRAMES
    
    # Call this to completely heal a monster
    def heal(self):
        self.state = self.last_state
        self.set_speed(Ghost.NORMAL_SPEED)
        

    def debug_target(self, debug):
        self.debug = debug


#---------------------------------- INTERNALS ---------------------------

def init_ghost_animations(sprite_sheet, ghost_row):
        row = 20*ghost_row+6
        animations = [ [], [], [], [], [], [], [], [], [], [] ]
        animations[Ghost.ANIM_UP].append(sprite_sheet.get_image(6, row, 16, 16))
        animations[Ghost.ANIM_UP].append(sprite_sheet.get_image(26, row, 16, 16))

        animations[Ghost.ANIM_DOWN].append(sprite_sheet.get_image(46, row, 16, 16))
        animations[Ghost.ANIM_DOWN].append(sprite_sheet.get_image(66, row, 16, 16))

        animations[Ghost.ANIM_LEFT].append(sprite_sheet.get_image(86, row, 16, 16))
        animations[Ghost.ANIM_LEFT].append(sprite_sheet.get_image(106, row, 16, 16))

        animations[Ghost.ANIM_RIGHT].append(sprite_sheet.get_image(126, row, 16, 16))
        animations[Ghost.ANIM_RIGHT].append(sprite_sheet.get_image(146, row, 16, 16))

        animations[Ghost.ANIM_FRIGHTENED].append(sprite_sheet.get_image(6, 8*20+6, 16, 16))
        animations[Ghost.ANIM_FRIGHTENED].append(sprite_sheet.get_image(26, 8*20+6, 16, 16))

        animations[Ghost.ANIM_RECOVERING].append(sprite_sheet.get_image(6, 8*20+6, 16, 16))
        animations[Ghost.ANIM_RECOVERING].append(sprite_sheet.get_image(26, 8*20+6, 16, 16))
        animations[Ghost.ANIM_RECOVERING].append(sprite_sheet.get_image(46, 8*20+6, 16, 16))
        animations[Ghost.ANIM_RECOVERING].append(sprite_sheet.get_image(66, 8*20+6, 16, 16))

        animations[Ghost.ANIM_DEAD_UP].append(sprite_sheet.get_image(6, 10*20+6, 16, 16))
        animations[Ghost.ANIM_DEAD_DOWN].append(sprite_sheet.get_image(26, 10*20+6, 16, 16))
        animations[Ghost.ANIM_DEAD_LEFT].append(sprite_sheet.get_image(46, 10*20+6, 16, 16))
        animations[Ghost.ANIM_DEAD_RIGHT].append(sprite_sheet.get_image(66, 10*20+6, 16, 16))

        return animations
