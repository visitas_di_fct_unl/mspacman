import base.colors
from ghosts.ghost import Ghost
from ghosts.ghost import init_ghost_animations

from base.spritesheet import SpriteSheet

class Pinky(Ghost):
    sprite_sheet = SpriteSheet("images/pacman-sprites.png")
    pinky_animations = init_ghost_animations(sprite_sheet, 5)

    def __init__(self, game, x, y):
        super().__init__(game, x,y,self.pinky_animations, (2*8+4,0+4))
        self.color = base.colors.PINK
