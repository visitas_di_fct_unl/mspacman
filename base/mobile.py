import pygame
import constants
from base.animated import Animated

class Mobile(Animated):
    """
    Mobile class
    
    Class for actors that have the ability to move during the game.
    A Mobile actor has a self.dir pair that describes in which direction to walk.
    """


    DIR_UP = (0, -1)
    DIR_DOWN = (0, 1)
    DIR_LEFT = (-1, 0)
    DIR_RIGHT = (1, 0)  
    DIR_NONE = (0, 0) 

    def __init__(self, game, x, y, animations):
        """
        Mobile constructor
        
        :param game: the game object
        :param x: the x position of the top left corner of the image on screen
        :param y: the y position of the top left corner of the image on screen
        :param animations: the set of sets of images that make up the animations 
        """
        super().__init__(game, x, y, animations)
        # The current direction of the mobile actor
        self.dir = Mobile.DIR_NONE
        self.pixels_per_frame = 1.0 # frames per pixel
        self.pixels_counter = 0.0
        # The list of obstacles against which it can collide
        self.obstacles = None

    def is_mobile(self):
        return True

    # Update operation on mobile objects call update_position() to actually do something
    # and then calls the update() from Animated, which will keep the animations playing
    # Each class specializing Mobile will have to program the do_work() operation

    def update(self):
        """
        update() operation for Mobile actors.
        
        This operation calls the update_position() of the Mobile actor, responsible for assigning
        self.dir with the appropriate value for its next move.
        After that, it calls the update() operation that comes from Animated class, which will
        update the images depending on current running animation
        """
        self.update_position()
        super().update()

    def face_dir(self, dir):
        """
        Sets the current direction of the actor.
        
        :param dir: a pair (dx,dy), where each dx and dy value is either 0, 1 or -1. 
            Only no move (0,0) and principal (up, down, left, right) directions are supported.
            Directions are represented by DIR_NONE, DIR_LEFT, DIR_RIGHT, DIR_UP and DIR_DOWN constants
        """
        self.dir = dir

    def face_left(self):
        """
        Sets DIR_LEFT as the current direction of the actor.
        """
        self.face_dir(Mobile.DIR_LEFT)

    def face_right(self):
        """
        Sets DIR_RIGHT as the current direction of the actor.
        """
        self.face_dir(Mobile.DIR_RIGHT)

    def face_up(self):
        """
        Sets DIR_UP as the current direction of the actor.
        """
        self.face_dir(Mobile.DIR_UP)
    
    def face_down(self):
        """
        Sets DIR_DOWN as the current direction of the actor.
        """
        self.face_dir(Mobile.DIR_DOWN)

    def is_facing_left(self):
        """
        Checks if the Mobile actor is facing left
        :return: True if actor is facing left, False otherwise
        """
        return self.dir == Mobile.DIR_LEFT
    
    def is_facing_right(self):
        """
        Checks if the Mobile actor is facing left
        :return: True if actor is facing left, False otherwise
        """
        return self.dir == Mobile.DIR_RIGHT

    def is_facing_up(self):
        """
        Checks if the Mobile actor is facing left
        :return: True if actor is facing left, False otherwise
        """
        return self.dir == Mobile.DIR_UP
    
    def is_facing_down(self):
        """
        Checks if the Mobile actor is facing left
        :return: True if actor is facing left, False otherwise
        """
        return self.dir == Mobile.DIR_DOWN

    def get_directions(self):
        """
        Gets all the directions that are possible moves for a Mobile actor. 
        These are the directions that will not make the actor collide with another one.
        
        :return: A list of all the directions that are collision free. Each direction is a pair (dx,dy)
            where dx and dy can either be 0, 1 or -1.
            If no move is possible in any of the 4 principal directions, DIR_NONE (0,0) 
            will be returned in the list.
        """

        # Start with an empty list
        directions = []

        #Check each direction and put it in the list

        # One particular detail in
        if self.can_go_left():
            directions.append(Mobile.DIR_LEFT)
        if self.can_go_right():
            directions.append(Mobile.DIR_RIGHT)
        if self.can_go_up():
            directions.append(Mobile.DIR_UP)
        if self.can_go_down():
            directions.append(Mobile.DIR_DOWN)

        if directions == []:
            directions.append(Mobile.DIR_NONE)

        return directions

    def set_speed(self, pixels_per_sec):
        """
        Sets the speed of the Mobile actor in pixels per second
        :param pixels_per_sec: speed of the actor in pixels per second.
        """
        self.pixels_per_frame = pixels_per_sec / constants.FPS





    """
    From this point onwards you can find some internal functions. Ignore them!
    """


    # Moves an actor by one pixel in the direction it is facing
    def move(self):
        """
        Moves a Mobile actor along its current direction.
        """
        self.pixels_counter = self.pixels_counter + self.pixels_per_frame
        pixels_to_move = int(self.pixels_counter)
        if pixels_to_move >= 0:
            self.rect.x = (self.rect.x + pixels_to_move * self.dir[0]) % constants.SCREEN_WIDTH
            self.rect.y = (self.rect.y + pixels_to_move * self.dir[1]) % constants.SCREEN_HEIGHT
            self.pixels_counter = self.pixels_counter - pixels_to_move

    def set_obstacles(self, obstacles):
        """
        Sets the list of obstacles agains which the actor needs to test for collisions.
        :param obstacles: list of actors that will work as obstacles.
        """
        self.obstacles = obstacles

    def can_go_ahead(self):
        """
        Checks if the actor can continue in its current direction.
        :return: True if the path ahead is clear, False otherwise.
        """
        return not self.will_collide(self.dir)

    def can_go_up(self):
        """
        Checks if the actor can continue in its current direction.
        :return: True if the path ahead is clear, False otherwise.
        """
        return not self.will_collide(Mobile.DIR_UP)

    def can_go_down(self):
        """
        Checks if the actor can continue in its current direction.
        :return: True if the path ahead is clear, False otherwise.
        """
        return not self.will_collide(Mobile.DIR_DOWN)

    def can_go_left(self):
        """
        Checks if the actor can continue in its current direction.
        :return: True if the path ahead is clear, False otherwise.
        """
        return not self.will_collide(Mobile.DIR_LEFT)

    def can_go_right(self):
        """
        Checks if the actor can continue in its current direction.
        :return: True if the path ahead is clear, False otherwise.
        """
        return not self.will_collide(Mobile.DIR_RIGHT)

    def will_collide(self, dir):
        """
        Checks if a collision exists in a certain direction.
        :param dir: the direction to test
        :return: True if it will collide along the given direction, False otherwise.
        """

        # Store the current position
        cur_x = self.rect.x
        cur_y = self.rect.y

        # Adjust the rectangle for a candidate new position
        self.rect.x = (self.rect.x + dir[0]) % constants.SCREEN_WIDTH
        self.rect.y = (self.rect.y + dir[1]) % constants.SCREEN_HEIGHT

        # Check if there are collisions with our obstacles at the candidate position
        hit_list = pygame.sprite.spritecollide(self, self.obstacles, False)

        # Return to the original position
        self.rect.x = cur_x
        self.rect.y = cur_y

        # Return true if the list of objects against which we have collided has at least one element
        return len(hit_list) > 0

    def collisions_list(self, group, destroy):
        """
        Returns the list of actors, from a given group (list) that are colliding with the actor.
        :param group: the list or group of actors to test against
        :param destroy: True if we want actors colliding with the actor to be removed from the group (destroyed),
            False if we want to keep them in the group.
        :return: the list of actors from the initial group that are colliding with the actor.
        """
        return pygame.sprite.spritecollide(self, group, destroy)