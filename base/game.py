import pygame


class Game:
    """
    Game class
    
    A class that provides an a framework to implement games.
    """
    def __init__(self, screen):
        """
        Game constructor.
        
        :param screen: the screen surface where the game will be displayed
        """
        self.screen = screen

    def initialize(self, caption, screen_size):
        """
        Initialization
        
        This operation is executed before the main loop.
        It is used to initialize the game, create the actors and start things rolling.
        :param caption: the caption of the window
        :param screen_size: the size of the surface where the game is displayed
        """
        self.surface = pygame.Surface(screen_size)
        pygame.display.set_caption(caption)

        # create a Clock to measure times
        clock = pygame.time.Clock()

        # When done is set to True, the game main loop will exit
        self.done = False

    def loop(self):
        """
        The game loop
        
        Every game runs a game loop, a particular sequence of operations 
        that are repeated until the end of the game.
        
        At every step the following sequence of operations will be called:
        i.   enter_step(), to do the work of updating all objects
        ii.  handle_event(), for each event in the event queue
        iii. draw(), to draw every actor of the game
        iv.  exit_step(), to finish the current step before entering a new one.
        """
        while not self.done:

            self.start_frame()

            # --- Main event loop
            for event in pygame.event.get():

                if event.type == pygame.QUIT:
                    done = True
                else:
                    self.handle_event(event)

            # Execute the draw operation
            self.draw()

            # Show the image that was created for this frame in a window
            pygame.transform.scale(self.surface, self.screen.get_size(), self.screen)
            pygame.display.flip()

            # Call the operation that finishes the current step
            self.end_frame()

    def handle_event(self):
        """
        Handle each event (keyboard, mouse, etc.) of the game.
        
        Do nothing in this class. Subclasses will implement what they need.
        """
        pass

    def draw(self):
        """
        Draw operation for the game.
        
        Do nothing in this class. Subclasses will implement what they need
        :return: 
        """
        pass