from pygame.sprite import Sprite

class Actor(Sprite):
    """
    Actor constructor
    """
    def __init__(self, game, x, y, image):
        """
        Actor constructor
        :param game: the game object
        :param x: the x position of the top left corner of the image on screen
        :param y: the y position of the top left corner of the image on screen
        :param image: the initial image to be used as sprite for this character
        """
        super().__init__()
        self.image = image
        self.rect = image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.game = game

    """
    The following functions determine the roles of each actor in the game
    In this class they are they represent the default (inherited) roles for all actors
    Each actor class (Monster, Food, etc.) will override this behaviours
    """

    def is_edible(self):
        """
        Checks if the actor is edible (may be eaten) by some other actor
        :return: True if the actor can be eaten, False otherwise
        """
        return False

    def is_obstacle(self):
        """
        Checks if the actor is some kind of obstacle to other actors' movements
        :return: True if the actor is an obstacle, False otherwise
        """
        return False

    def is_killer(self):
        """
        Checks if the the actor is capable of "killing" the hero
        :return: True if the actor is a "killer", False otherwise
        """
        return False

    def is_mobile(self):
        """
        Checks if the actor is mobile (capable of move around)
        :return: True if the actor is mobile, False otherwise
        """
        return False

    def is_powerup(self):
        """
        Checks if the actor is a powerup item given special abilities to the hero
        :return: True if the actor is a powerup, False otherwise
        """
        return False

    def position(self):
        """
        Gives the position (in pixels) of the actor
        :return: a pair (x,y) with the x and y coordinates of the actor's center
        """
        return (self.rect.x + 4, self.rect.y + 4)
