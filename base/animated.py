import pygame
from base.actor import Actor
import constants

class Animated(Actor):
    """
    Animated class
    
    A class for characters that have several sequences of images like runing, jumping, walking left, etc.
    """
    
    # Number of frames that need to elapse before going to the next image in the current animation.
    FRAMES_PER_SPRITE=6

    def __init__(self, game, x, y, animations):
        """
        Animated constructor
        
        :param game: the game object
        :param x: the x position of the top left corner of the image on screen
        :param y: the y position of the top left corner of the image on screen
        :param animations: the set of sets of images that make up the animations 
        """
        super().__init__(game, x,y,animations[0][0])
        
        self.animations = animations
        self.curr_anim = None
        self.playing = False
        self.frames_counter = 0
        self.rect = pygame.Rect(x+4,y+4,8,8)

    def start_animation(self, name):
        """
        Starts playing an animation
        :param name: the animation to be played
        """
        # if we are already in the right animation, just return from the function
        if(self.curr_anim == name):
            return

        # set the current animation
        self.curr_anim = name
        # reset to the first image in the sequence
        self.img_index = 0
        # remember that the actor is now playing an animation
        self.playing = True
        # update the current image
        self.update_image()

    def pause_animation(self):
        """
        Stops playing the current animation.
        """
        self.playing = False

    def resume_animation(self):
        """
        Resumes playing the current animation.
        """
        self.playing = True

    def update(self):
        """
        Update operation.
        
        This function is called at every frame of our game for each Animated object.
        Animated objects use it to update the current image of the playing animation.
        """
        self.update_animation()
    
    def draw(self, screen):
        """
        Draw operation.
        
        This function overrides the default draw operation of a Sprite.
        In this case, since our Animated actors are 16x16 but are centered on 8x8 tiles
        the top left corner starts 4 pixels to the left and 4 pixels up.
        :param screen: the drawing surface where the actor will be drawn
        """
        screen.blit(self.image, (self.rect.x-4,self.rect.y-4))









    """
    From this point onwards you can find some internal functions. Ignore them!
    """
    def update_animation(self):
        """
        Updates the current animation, if needed
        """
        if not self.playing:
            return
        self.frames_counter = self.frames_counter + 1
        if(self.frames_counter == Animated.FRAMES_PER_SPRITE):
            self.frames_counter = 0
            self.next_sprite()
            
    def next_sprite(self):
        """
        Advances the current animation by one frame
        """
        self.img_index = (self.img_index + 1) % len(self.animations[self.curr_anim])
        self.update_image()

    def update_image(self):
        """
        Refresh the current image (the one that will be used for drawing)
        """
        self.image = self.animations[self.curr_anim][self.img_index]
