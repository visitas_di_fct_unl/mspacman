
""" All things related to the levels of the game """

import pygame
import constants
from base.spritesheet import SpriteSheet

from tiles.wall import Wall
from tiles.food import Food
from tiles.door import Door
from tiles.energizer import Energizer

# the list of levels
LEVELS = [
    [ 
        "QWWWWWWWWWWWWBNWWWWWWWWWWWWE", 
        "A............ad............D", 
        "A.qwwe.qwwwe.ad.qwwwe.qwwe.D", 
        "AOa  d.a   d.ad.a   d.a  dOD", 
        "A.zxxc.zxxxc.zc.zxxxc.zxxc.D", 
        "A..........................D",
        "A.qwwe.qe.qwwwwwwe.qe.qwwe.D",
        "A.zxxc.ad.zxxjkxxc.ad.zxxc.D",
        "A......ad....ad....ad......D",
        "ZXXXXe.aiwwe ad qwwud.qXXXXC",
        "     A.akxxc zc zxxjd.D     ",
        "     A.ad          ad.D     ",
        "     A.ad UXXPPXXI ad.D     ",
        "WWWWWc.zc D--  --A zc.zWWWWW",
        "      .   D      A   .      ",
        "XXXXXe.qe D------A qe.qXXXXX",
        "     A.ad JWWWWWWK ad.D     ",
        "     A.ad          ad.D     ",
        "     A.ad qwwwwwwe ad.D     ",
        "QWWWWc.zc zxxjkxxc zc.zWWWWE",
        "A............ad............D",
        "A.qwwe.qwwwe.ad.qwwwe.qwwe.D",
        "A.zxjd.zxxxc.zc.zxxxc.akxc.D",
        "AO..ad.......  .......ad..OD",
        "Twe.ad.qe.qwwwwwwe.qe.ad.qwY",
        "Gxc.zc.ad.zxxjkxxc.ad.zc.zxH",
        "A......ad....ad....ad......D",
        "A.qwwwwuiwwe.ad.qwwuiwwwwe.D",
        "A.zxxxxxxxxc.zc.zxxxxxxxxc.D",
        "A..........................D",
        "ZXXXXXXXXXXXXXXXXXXXXXXXXXXC"
    ]
]

def build_images():
    sheet = SpriteSheet("images/levelitems.png")
    items = {}

    items["Q"] = sheet.get_image(0,0,8,8)   # Double outer corners
    items["W"] = sheet.get_image(8,0,8,8)   # Q W E
    items["E"] = sheet.get_image(216,0,8,8) # A   D
    items["A"] = sheet.get_image(0,8,8,8)   # Z X E
    items["D"] = sheet.get_image(216,8,8,8)
    items["Z"] = sheet.get_image(0,72,8,8)
    items["X"] = sheet.get_image(8,72,8,8)
    items["C"] = sheet.get_image(216,72,8,8)
    
    items["q"] = sheet.get_image(16,16,8,8) # Single outer corners
    items["w"] = sheet.get_image(24,16,8,8) # q w e
    items["e"] = sheet.get_image(40,16,8,8) # a   d
    items["a"] = sheet.get_image(16,24,8,8) # z x c
    items["d"] = sheet.get_image(112,8,8,8)
    items["z"] = sheet.get_image(16,32,8,8)
    items["x"] = sheet.get_image(8,200,8,8)
    items["c"] = sheet.get_image(40,32,8,8)
    
    items["."] = sheet.get_image(8,8,8,8)   # food
    
    items["O"] = sheet.get_image(8,24,8,8)  # Energizer
    
    items[" "] = sheet.get_image(24,24,8,8) # empty tile
    
    items["-"] = sheet.get_image(24,24,8,8) # invisible wall
    
    items["u"] = sheet.get_image(152,72,8,8) # Single inner corners
    items["i"] = sheet.get_image(64,72,8,8)
    items["j"] = sheet.get_image(104,56,8,8)
    items["k"] = sheet.get_image(112,56,8,8)
    
    items["U"] = sheet.get_image(80,96,8,8) # Corners of the ghosts' house
    items["I"] = sheet.get_image(136,96,8,8)
    items["J"] = sheet.get_image(80,128,8,8)
    items["K"] = sheet.get_image(136,128,8,8)
    
    items["P"] = sheet.get_image(104,96,8,8) # Door in the ghost's house
    
    items["T"] = sheet.get_image(0,192,8,8) # Special tiles with double and single lines
    items["G"] = sheet.get_image(0,200,8,8)
    items["Y"] = sheet.get_image(216,192,8,8)
    items["H"] = sheet.get_image(216,200,8,8)
    items["B"] = sheet.get_image(104,0,8,8)
    items["N"] = sheet.get_image(112,0,8,8)
    
    return items

'''
Auxiliary function to create a specific actor from the level map
'''
def build_actor(game, kind, x, y, image):
    k = str.lower(kind)
    if k=="q" or k=="w" or k=="e" or k=="a" or k=="d" or k=="z" or k=="x" or k=="c": return Wall(game, x,y,image)
    if k=="u" or k=="i" or k=="j" or k=="k": return Wall(game, x,y,image)
    elif kind=="T" or kind=="G" or kind=="Y" or kind=="H" or kind=="B" or kind=="N": return Wall(game, x,y,image)
    elif kind=="P": return Door(game, x,y,image)
    elif kind==".": return Food(game, x,y,image)
    elif kind=="O": return Energizer(game, x,y,image)
    elif kind=="-": return Wall(game, x,y,image)
    elif kind==" ": return None

class Level:
    # static member variables
    images = build_images()

    def __init__(self, level):
        self.level = level
        # Always the same level... Replace 0 with self.level and add more levels to LEVELS above
        self.map = LEVELS[0]

    def build(self, game):
        sprites = []
        for i in range(0,len(self.map)):
            for j in range(0,len(self.map[i])):
                kind = self.map[i][j]
                sprite = build_actor(game, kind,
                                     constants.GRID_CELL_WIDTH*j, 
                                     constants.GRID_CELL_WIDTH*(i+constants.TOP_LINES), 
                                     self.images[kind])
                if sprite != None: sprites.append(sprite)

        return sprites

