import pygame
from base.game import Game

import constants
import base.colors


screen_size = (constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT)
window_size = (constants.SCREEN_WIDTH*constants.ZOOM, constants.SCREEN_HEIGHT*constants.ZOOM)

pygame.init()
window = pygame.display.set_mode(window_size, pygame.RESIZABLE)

from levels import Level

from mspacman import MsPacman
from ghosts.blinky import Blinky
from ghosts.pinky import Pinky
from ghosts.inky import Inky
from ghosts.clyde import Clyde

from pygame.sprite import Group

class PacGame(Game):
    def __init__(self, window):
        super().__init__(window)

    def initialize(self, screen_size):
        super().initialize("Ms. Pac-Man", screen_size)
        # Used to manage how fast the screen updates and control the timer
        self.clock = pygame.time.Clock()

        # Start by creating our major characters at their intended locations

        # Our hero first
        self.hero = MsPacman(self, 104, 204)

        # Our opponents after...
        self.blinky = Blinky(self, 104, 108)  # The red ghost
        self.inky = Inky(self, 88, 136)  # The cyan ghost
        self.pinky = Pinky(self, 104, 136)  # The pink ghost
        self.clyde = Clyde(self, 120, 136)  # The orange ghost

        # Create the level for map 0 (in fact, we only have on map)
        self.level = Level(0)

        # Get the list of all the static objects that make up the level
        sprites_list = self.level.build(self)

        # Initialize several lists of actors

        # The list containing all the mobile characters
        self.mobiles = [self.hero, self.blinky, self.pinky, self.inky, self.clyde]
        # The list containing all the characters that can kil our hero
        powerups = []

        # Build groups of objects for collision detection (pygame uses a Group to handle collisions)
        obstacles = Group()  # The group of objects that mobile actors cannot cross
        edibles = Group()  # The group of objects that we can eat
        self.static = Group()  # The group of objects that
        self.ghosts = Group()
        self.ghosts.add(self.blinky, self.pinky, self.inky, self.clyde)

        for sprite in sprites_list:
            if sprite.is_edible():
                edibles.add(sprite)
            #if sprite.is_powerup():
            #    powerups.append(sprite)
            if sprite.is_obstacle():
                obstacles.add(sprite)
            if sprite.is_mobile():
                self.mobiles.append(sprite)
            self.static.add(sprite)

        self.edible_items = len(edibles)
        self.eaten_edibles = 0

        self.hero.set_edibles(edibles)
        self.hero.set_killers(self.ghosts)

        for mobile in self.mobiles:
            mobile.set_obstacles(obstacles)

        self.blinky.unlock()

        self.timer = 0

        self.keystate = {pygame.K_UP: False, pygame.K_DOWN: False, pygame.K_LEFT: False, pygame.K_RIGHT: False,
                    pygame.K_ESCAPE: False, pygame.K_x: False}

    def start_frame(self):
        # --- Game logic should go here
        # First, let's deal with the global game logic
        if self.timer == 7*constants.FPS:
            # Switch to chase mode
            self.chase_mode()
        elif self.timer == 27*constants.FPS:
            # Switch to scatter mode
            self.scatter_mode()
        elif self.timer == 34*constants.FPS:
            # Back to chase mode
            self.chase_mode()
        elif self.timer == 54*constants.FPS:
            # scatter mode once again
            self.scatter_mode()
        elif self.timer == 59*constants.FPS:
            # chase mode
            self.chase_mode()
        elif self.timer == 79*constants.FPS:
            # scatter mode
            self.scatter_mode()
        elif self.timer == 84*constants.FPS:
            # final chase mode
            self.chase_mode()

        # Now give a change for each mobile actor to do what it wants
        for m in self.mobiles:
            m.update()


    def handle_event(self, event):
        if event.type == pygame.KEYDOWN:
            self.keystate[event.key] = True
        elif event.type == pygame.KEYUP:
            self.keystate[event.key] = False

        if self.keystate[pygame.K_ESCAPE] == True:
            self.done = True

        self.hero.handle_keyboard(self.keystate)

    def draw(self):
        # First, clear the screen to black.
        self.surface.fill(base.colors.BLACK)

        # draw all static actors
        self.static.draw(self.surface)

        # draw all the mobile actors
        for m in self.mobiles:
            m.draw(self.surface)

    def end_frame(self):
        self.clock.tick(constants.FPS)
        # we only update the timer if no ghost is frightened
        for ghost in self.ghosts:
            if ghost.is_frightened():
                return

        self.timer = self.timer + 1

    def chase_mode(self):
        for ghost in self.ghosts:
            ghost.chase()

    def scatter_mode(self):
        for k in self.ghosts:
            k.scatter()

    def edible_eaten(self):
        """
        This is called every time the hero eats a food item.
        Since other ghosts are released depending on the amount
        of food the hero has already eaten, this is the right place
        to release the other ghosts
        """

        # Count food items eaten
        self.eaten_edibles = self.eaten_edibles + 1
        # pinky is released immediately
        if self.eaten_edibles == 1:
            self.pinky.unlock()
        # inky is released after 30 food items
        elif self.eaten_edibles == 30:
            self.inky.unlock()
        # finally, clyde is released after 72 (=30% of 240) food items
        elif self.eaten_edibles == 72:
            self.clyde.unlock()

    def energizer_eaten(self):
        """
        No use for this for the moment...
        """
        pass

    def ghosts_house_pos(self):
        return (104, 136)

game = PacGame(window)
game.initialize(screen_size)
game.loop()
pygame.quit()