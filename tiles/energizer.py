from tiles.food import Food

class Energizer(Food):
    FOOD_POINTS = 10
    def __init__(self, game, x, y, image):
        super().__init__(game, x,y,image)

    def eat(self): return self.FOOD_POINTS

    def is_powerup(self): return True
