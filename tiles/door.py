from base.actor import Actor

class Door(Actor):
    def __init__(self, game, x, y, image):
        super().__init__(game, x,y,image)

    def is_obstacle(self): return True
