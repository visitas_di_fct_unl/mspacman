from base.actor import Actor

class Food(Actor):
    FOOD_POINTS = 1
    def __init__(self, game, x, y, image):
        super().__init__(game, x,y,image)

    def is_edible(self):
        return True

    def eat(self):
        return self.FOOD_POINTS
