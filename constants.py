# Number of frames (complete new image) per second
FPS=45
# Number of frames spent on each sprite of the animations
FRAMES_PER_SPRITE=6

MAP_WIDTH=28
MAP_HEIGHT=31

TOP_LINES = 3
BOTTOM_LINES = 2
# screen dimensions in tiles (the actual level map is smaller in height)
GRID_WIDTH=MAP_WIDTH
GRID_HEIGHT=MAP_HEIGHT + TOP_LINES + BOTTOM_LINES

# Dimension of each tile
GRID_CELL_WIDTH=8
GRID_CELL_HEIGHT=8

# Screen dimensions
SCREEN_WIDTH=GRID_WIDTH*GRID_CELL_WIDTH
SCREEN_HEIGHT=GRID_HEIGHT*GRID_CELL_HEIGHT

# Zoom factor
ZOOM=2