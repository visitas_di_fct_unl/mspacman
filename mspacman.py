import pygame
import constants
from base.mobile import Mobile
from base.spritesheet import SpriteSheet

LEFT = 0
RIGHT = 1
UP = 2
DOWN = 3
DYING = 4
SPRITE_ADJUST_X = 0
SPRITE_ADJUST_Y = 0
MS_PACMAN_OFFSET = 80


def init_mspacman_animations():
        sprite_sheet = SpriteSheet("images/pacman-sprites.png")
        animations = [ [], [], [], [], [] ]
        animations[RIGHT].append(sprite_sheet.get_image(6+MS_PACMAN_OFFSET, 26+SPRITE_ADJUST_Y, 16, 16))
        animations[RIGHT].append(sprite_sheet.get_image(26+MS_PACMAN_OFFSET, 26+SPRITE_ADJUST_Y, 16, 16))

        animations[LEFT].append(sprite_sheet.get_image(6+MS_PACMAN_OFFSET, 6+SPRITE_ADJUST_Y, 16, 16))
        animations[LEFT].append(sprite_sheet.get_image(26+MS_PACMAN_OFFSET, 6+SPRITE_ADJUST_Y, 16, 16))

        animations[UP].append(sprite_sheet.get_image(6+MS_PACMAN_OFFSET, 46+SPRITE_ADJUST_Y, 16, 16))
        animations[UP].append(sprite_sheet.get_image(26+MS_PACMAN_OFFSET, 46+SPRITE_ADJUST_Y, 16, 16))

        animations[DOWN].append(sprite_sheet.get_image(6+MS_PACMAN_OFFSET, 66+SPRITE_ADJUST_Y, 16, 16))
        animations[DOWN].append(sprite_sheet.get_image(26+MS_PACMAN_OFFSET, 66+SPRITE_ADJUST_Y, 16, 16))

        for i in range(0,18):
            animations[DYING].append(sprite_sheet.get_image(6+i*20, 246+SPRITE_ADJUST_Y, 16, 16))
        return animations

class MsPacman(Mobile):
    mspacman_animations = init_mspacman_animations()

    NORMAL_SPEED = constants.FPS
    SLOW_SPEED = 0.6*constants.FPS

    def __init__(self, game, x, y):
        super().__init__(game, x, y, self.mspacman_animations)
        self.wants_to_go_up = False
        self.wants_to_go_down = False
        self.wants_to_go_left = False
        self.wants_to_go_right = False
        self.obstacles = None
        self.game = game
        self.set_speed(MsPacman.NORMAL_SPEED)

    def set_edibles(self, edibles):
        self.edibles = edibles

    def set_killers(self, killers):
        self.killers = killers

    def handle_keyboard(self, keystate):
        self.wants_to_go_up    = keystate[pygame.K_UP]
        self.wants_to_go_down  = keystate[pygame.K_DOWN]
        self.wants_to_go_left  = keystate[pygame.K_LEFT]
        self.wants_to_go_right = keystate[pygame.K_RIGHT]

        for ghost in self.killers:
            ghost.debug_target(keystate[pygame.K_x])


    def die(self):
        self.start_animation(DYING)
        self.face_dir(Mobile.DIR_NONE)

    def is_dead(self):
        return self.curr_anim == DYING

    # GIRLS IN ICT
    def update_position(self):
        # If Pac is busy dying no need to update its position
        if self.is_dead():
            return

        # try moving up
        if self.wants_to_go_up and self.can_go_up(): 
            self.face_up()
            self.start_animation(UP)

        # try moving down
        if self.wants_to_go_down and self.can_go_down(): 
            self.face_down()
            self.start_animation(DOWN)

        # try moving left
        if self.wants_to_go_left and self.can_go_left(): 
            self.face_left()
            self.start_animation(LEFT)

        #try moving right
        if self.wants_to_go_right and self.can_go_right(): 
            self.face_right()
            self.start_animation(RIGHT)

        # really move... if we can...
        if self.can_go_ahead():
            self.move()
        else:
            self.pause_animation()

        # adjust mspacman position (use the level as a cylinder)
        self.rect.x = self.rect.x % constants.SCREEN_WIDTH

        # lunch time
        victims = self.collisions_list(self.edibles, True)
        for v in victims:
            # test if it gives the mspacman the ability to frighten the ghosts
            if v.is_powerup():
                # inform game that an energizer was eaten (to stop the timer)
                self.game.energizer_eaten()
                # go through each monster
                for killer in self.killers:
                    # and frighten it
                    killer.frighten()
            else:
                self.game.edible_eaten()

        victims = self.collisions_list(self.killers, False)
        for v in victims:
            if(v.is_frightened()):
                    v.die()
            elif not v.is_dead():
                self.die()
